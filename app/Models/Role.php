<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as ModelRole;

class Role extends ModelRole
{
    use HasFactory;

    protected $fillable =[
      'name',
        'display_name',
        'group',
    ];
}
