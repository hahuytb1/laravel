<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as ModelPermission;

class Permission extends ModelPermission
{
    use HasFactory;

    protected $fillable =[
        'name',
        'display_name',
        'group',
    ];
}
